#include "pwm_driver.h"
#include "shakti_arduino.h"

#define PERIOD_REGISTER_MAX     0x0000FFFF 
#define DUTY_REGISTER_MAX       0x0000FFFF 
#define CLOCK_REGISTER_MAX      0x0000FFFF
#define CONTROL_REGISTER_MAX    0x000000FF

void tone(int pin, unsigned int frequency, unsigned long duration)
{
    uint32_t clock_divisor_value = CLOCK_FREQUENCY/frequency;
    pwm_configure(PWM_0, clock_divisor_value, 0xf0, 0x80, false);

    if(duration)
    {
        pwm_start(pin,0);
        delay(duration);
        pwm_stop(pin);
    }
    else
    {
        pwm_start(pin,0);
    }
}

void noTone(int pin)
{
    pwm_stop(pin);
}